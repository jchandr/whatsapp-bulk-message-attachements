from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from time import sleep
from urllib.parse import quote
import os
import re
import csv

os.system("")
os.environ["WDM_LOG_LEVEL"] = "0"
class style():
	BLACK = '\033[30m'
	RED = '\033[31m'
	GREEN = '\033[32m'
	YELLOW = '\033[33m'
	BLUE = '\033[34m'
	MAGENTA = '\033[35m'
	CYAN = '\033[36m'
	WHITE = '\033[37m'
	UNDERLINE = '\033[4m'
	RESET = '\033[0m'

f = open("message.txt", "r", encoding="utf8")
message = f.read()
f.close()

print(style.YELLOW + '\nThis is your message-')
print(style.GREEN + message)
print("\n" + style.RESET)
message = quote(message)

delay = 30

driver = webdriver.Chrome('./chromedriver.exe')
print('Once your browser opens up sign in to web whatsapp')
driver.get('https://web.whatsapp.com')
input(style.MAGENTA + "AFTER logging into Whatsapp Web is complete and your chats are visible, press ENTER..." + style.RESET)

with open('./bulk-export.csv', newline='') as csvfile:
	spamreader = csv.reader(csvfile, delimiter=',', quotechar='"')
	for row in spamreader:
		nums = re.findall('[1-9]{1}[0-9]{9}', row[2])
		for number in nums:
			print(style.YELLOW + 'Sending message to {}.'.format(number) + style.RESET)
			try:
				url = 'https://web.whatsapp.com/send?phone=' + number + '&text=' + message
				sent = False
				for i in range(3):
					if not sent:
						driver.get(url)
						try:
							try:
								phoneNotAvailable = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, "//div[@data-testid='popup-contents'][contains(text(),\'Phone number shared via url is invalid.')]")))
								print(phoneNotAvailable)
								if(phoneNotAvailable):
									print('Phone Number Not Available On Whatsapp')
									# phoneNotAvailableOkButton = WebDriverWait(driver, delay).until(EC.element_to_be_clickable((By.XPATH, "//button[@data-testid='popup-controls-ok']")))
									# phoneNotAvailableOkButton.click()
									break
							except Exception as e2:
								print('here')
								attachButton = WebDriverWait(driver, delay).until(EC.element_to_be_clickable((By.XPATH, "//div[@role='button'][@title='Attach']")))
								attachButton.click()
								inputTag = driver.find_element(By.XPATH, "//input[@type='file'][@accept='image/*,video/mp4,video/3gpp,video/quicktime']")
								inputTag.send_keys(os.getcwd() + '/image.jpeg')
								sendButton = WebDriverWait(driver, delay).until(EC.element_to_be_clickable((By.XPATH, "//div[@role='button'][@aria-label='Send']")))
								sleep(5)
								sendButton.click()
						except Exception as e1:
							print(e1)
							print(style.RED + f"\nFailed to send message to: {number}, retry ({i+1}/3)")
							print("Make sure your phone and computer is connected to the internet.")
							print("If there is an alert, please dismiss it." + style.RESET)
						else:
							sleep(3)
							sent = True
							print(style.GREEN + 'Message sent to: ' + number + style.RESET)
			except Exception as e:
				print(style.RED + 'Failed to send message to ' + number + str(e) + style.RESET)

driver.close()
